context("Checking uniform implementation between stan and C functions")
if( "rstan" %in% installed.packages()[,1] ){
    library(rstan)
    
    if(file.exists("../../inst")){
        unifed.stan.folder.path <- "../../inst/stan/"
    }else{
        unifed.stan.folder.path <- "../../unifed/stan/"
    }
    
    stan.model.string <- paste("functions{",
                               do.call(function(...)(paste(...,sep="\n")),
                                       as.list(readLines(paste0(unifed.stan.folder.path,"unifed.stan")))),
                               "}")

    
    stanmodel.list <- stanc(model_code=stan.model.string)
    

    expose_stan_functions(stanmodel.list)

    test_that("Checks for kappa",{
        xs <- -100:100
        for(x in xs)
            expect_equal(unifed.kappa(x),unifed_kappa(x))
    })

    test_that("Checks for the derivative of kappa",{
        xs <- -100:100
        for(x in xs)
            expect_equal(unifed.kappa.prime(x),unifed_kappa_prime(x))
    })

    test_that("Checks for the second derivative of kappa",{
        xs <- -100:100
        for(x in xs)
            expect_equal(unifed.kappa.double.prime(x),unifed_kappa_double_prime(x))
    })

    test_that("Checks for the inverse of the first derivative of kappa",{
        xs <- seq(0.001,1,length.out=1000)
        for(x in xs)
            expect_equal(unifed.kappa.prime.inverse(x),unifed_kappa_prime_inverse(x))
    })

    test_that("Checks consitency of cumulated function",{
        for( x in seq(0,1,length.out=100))
            for( theta in c(-1000,-50,0,0,1,100,100))
                expect_equal(punifed(x,theta),exp(unifed_lcdf(x,theta)))
    })

    test_that("Checks consitency of the unit deviance",{
        N <- 100
        y <- runif(N)
        mu <- runif(N)
        for(i in 1:N)
            expect_equal(unifed.unit.deviance(y[i],mu[i]),unifed_unit_deviance(y[i],mu[i]))
    })

}
